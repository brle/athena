#include "PixelToTPIDTool/PixelToTPIDTool.h"
#include "PixelToTPIDTool/PixeldEdxTestAlg.h"
#include "PixelToTPIDTool/PixeldEdxAODFix.h"

DECLARE_COMPONENT( InDet::PixelToTPIDTool )
DECLARE_COMPONENT( PixeldEdxTestAlg )
DECLARE_COMPONENT( InDet::PixeldEdxAODFix )
 
